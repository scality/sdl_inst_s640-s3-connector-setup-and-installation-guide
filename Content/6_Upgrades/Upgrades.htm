﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1><a name="top"></a>Upgrades</h1>
        <p>The <MadCap:variable name="S3 Connector Variables.ComponentName" /> software version can be upgraded using the same Federation playbooks used for the initial deployment of the S3 Connector. As described in Modifying the Inventory Files in this guide, Federation uses the Inventory file as the basis for deploying the <MadCap:variable name="S3 Connector Variables.ComponentName" /> containers to the specified target machines in your environment. The same mechanism is used to upgrade your <MadCap:variable name="S3 Connector Variables.ComponentName" /> to newer versions, as described below.</p>
        <p>To minimize impact on your Scality S3 service, the <MadCap:variable name="S3 Connector Variables.ComponentName" /> supports online “rolling” upgrades of the underlying containers across the machines hosting these services. Rolling upgrades ensure that the system is upgraded incrementally across the containers and machines in your environment. With the scale-out capabilities of the S3 Connector, this process ensures that there are available S3 endpoints and underlying services to respond to incoming requests during the upgrade process, across the system.</p>
        <p>To perform a rolling upgrade of your S3 Connector, you must update servers one by one. To do this, limit the installation to one server with <code>-l $host_ip</code>. Here are examples of the command using rolling upgrade.</p>
        <p>With internet access:</p><pre xml:space="preserve">$ ansible-playbook -l 192.168.0.1 -i env/server-pool-1/inventory run.yml</pre>
        <p>Without internet access:</p><pre>$&#160;./ansible-playbook -l 192.168.0.1 -i env/server-pool-1/inventory run.yml</pre>
        <p>As is standard in the world of cloud storage, applications must be prepared to handle response code 500s returned by the S3 endpoints from client requests. In the case of rolling upgrades, there is a small window of time when requests for access to Buckets may return a 500 response code. This happens only when the Metadata leader container itself is being upgraded. That request may be retried successfully as soon as the container has completed its upgrade, typically within tens of seconds. </p>
        <p>Note that with the use of an external load balancer, customers may also deploy the S3 healthcheck facility provided as part of the <MadCap:variable name="S3 Connector Variables.ComponentName" />. This can determine the up/down status of a specific S3 endpoint proactively as a part of the load balancer’s periodic health checks of all the endpoints. If an endpoint responds with a 500 response code, that endpoint may be currently upgrading, and therefore traffic should not be sent to it. This can help further reduce the time window where response code 500s are sent back to clients during the upgrade process.</p>
        <h2><a name="Upgrade3"></a>Upgrade from 6.4.X to 6.4.7.X</h2>
        <p>Minor versions of S3 6.4.7 improve stability and security and ease installation. These versions are referred to here as 6.4.0.X, with <i>X</i> a number incrementing from 1. In the following procedure, replace <i>X</i> with the minor revision number.</p>
        <p>This procedure temporarily stops services in the connectors. Hence, you must ensure the connectors are temporarily deactivated  in either the load balancers or the DNS configuration before upgrading them. Upgrade them separately to ensure service is not interrupted.</p>
        <h3>Download and Extract Offline Installer</h3>
        <ol>
            <li>Download the s3-offline-GA6.4.7.tar.bz2 archive from <a href="http://packages.scality.com/S3">our repository</a>. Put it in the same directory as the old s3-offline-GA6.4.Y/ directory.</li>
            <li>
                <p>Extract it:</p><pre>$ tar -xvjf s3-offline-GA6.4.7.tar.bz2 </pre>
            </li>
        </ol>
        <h3>Copy the Old Environment Files</h3>
        <p>Copy the old environment directory to the new installer directory:</p><pre>$ cp -r s3-offline-GA6.4.Y/federation/env/{{ EnvDir }} s3-offline-GA6.4.7/federation/env </pre>
        <h3>Modify group_vars/all</h3>
        <p>Open the s3-offline-GA6.4.7/env/{{ EnvDir }}/group_vars/all file and change the version:</p><pre>env_deploy_version: GA6.4.7</pre>
        <h3><a name="Stop"></a>Stop Services</h3>
        <p>The ntpd, httpd, and chronyd services may negatively affect the upgrade. Stop them temporarily in the S3 connectors with:</p><pre xml:space="preserve">$ sudo systemctl stop ntpd httpd chronyd </pre>
        <p>You will restart these services at the end of the upgrade.</p>
        <h3>Prepare the Connectors Upgrade</h3>
        <p>Navigate to s3-offline-GA6.4.7/federation/ directory and set the ENV_DIR environment variable with the name of your environment.</p><pre xml:space="preserve">$ cd s3-offline-GA6.4.7/federation </pre><pre xml:space="preserve">$ ENV_DIR={{ EnvDir }} </pre>
        <h3>Upgrade the Stateful-Only Connectors</h3>
        <ol>
            <li>
                <p>List the address of the stateful-only hosts:</p><pre xml:space="preserve">$ ../repo/venv/bin/ansible --list-hosts -i env/${ENV_DIR}/inventory 'runners_metadata:!runners_s3'
					
hosts (2):
address1
address2 </pre>
                <p>The length of the resulting list depends on your architecture. If it is empty, skip the next step and go to the next section (<MadCap:xref href="#Upgrade">“Upgrade the Remaining Stateful Connectors” on page&#160;1</MadCap:xref>.)</p>
            </li>
            <li>
                <p style="page-break-after: avoid;">Upgrade each listed connector:</p><pre>$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address1
$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address2
$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address3 [...] </pre>
                <p>If any of these commands fails, stop. Contact Scality support.</p>
            </li>
        </ol>
        <h3><a name="Upgrade"></a>Upgrade the Remaining Stateful Connectors</h3>
        <ol>
            <li>
                <p>List the address of the remaining stateful connectors:</p><pre xml:space="preserve">$ ../repo/venv/bin/ansible --list-hosts -i env/${ENV_DIR}/inventory 'runners_metadata:&amp;runners_s3'

hosts (3): 
address3
address4
address5 </pre>
                <p>The length of the resulting list depends on your architecture. If it is empty, skip the next step and go to the next section (<MadCap:xref href="#Upgrade2">“Upgrade the Stateless-Only Connectors” on page&#160;1</MadCap:xref>.)</p>
            </li>
            <li>
                <p>Deactivate each listed connector in the load balancer or the DNS entries to ensure it cannot receive any traffic, then upgrade each connector:</p><pre>$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address3
$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address4
$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address5 [...] </pre>
                <p>If any of these commands fails, stop. Contact Scality support.</p>
            </li>
        </ol>
        <h3><a name="Upgrade2"></a>Upgrade the Stateless-Only Connectors</h3>
        <p>List the address of the stateless-only connectors:</p><pre>$ ../repo/venv/bin/ansible --list-hosts -i env/${ENV_DIR}/inventory 'runners_s3:!runners_metadata'
			
hosts (2):
address6
address7</pre>
        <p>The length of the resulting list depends on your architecture. If it is empty, skip the next step and go to the next section (<MadCap:xref href="#Start">“Start ntpd and httpd” on page&#160;1</MadCap:xref>.)</p>
        <ol>
            <li>
                <p>Deactivate each listed connector in the load balancer or the DNS entries to ensure it cannot receive any traffic, then upgrade each connector:</p><pre>$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address6
$ ./ansible-playbook -i ./env/${ENV_DIR}/inventory run.yml --limit address7 [...] </pre>
                <p>If any of these commands fails, stop. Contact Scality support.</p>
            </li>
        </ol>
        <h3><a name="Start"></a>Start Services</h3>
        <p style="page-break-after: avoid;">Start the ntpd, httpd and chronyd services you stopped in <MadCap:xref href="#Stop">“Stop ntpd and httpd” on page&#160;1</MadCap:xref>.</p><pre xml:space="preserve">$ sudo systemctl start ntpd httpd chronyd</pre>
        <p>Verify that the service has started correctly:</p><pre>$ sudo timedatectl status</pre>
        <h2>Upgrading to <MadCap:variable name="S3 Connector Variables.ComponentName" /> 7.0</h2>
        <p class="NoteUnder">In major release R7.0 of the <MadCap:variable name="S3 Connector Variables.ComponentName" />, the system is upgraded to use a Docker version (1.13) that supports online upgrades of the Docker environment itself. The Federation process manages this upgrade automatically. Earlier Docker versions do not support online upgrades, increasing the time during which 500 response codes  may be returned during an upgrade. Once the product is upgraded to R7.0, this issue will not recur, as all future versions of Docker will support online upgrades.</p>
        <h3 MadCap:conditions="PrintGuides.GA7.0" class="noTOC">Warning</h3>
        <p MadCap:conditions="PrintGuides.GA7.0">If upgrading to release 7.0 of the <MadCap:variable name="S3 Connector Variables.ComponentName" /> with existing buckets that have locations set (such as <span class="Variable">us-east-1</span>), those locations must be listed in the location_constraints section of the Group Variables (all) File (see <MadCap:xref href="../2_Deployment_Environment/Modifying the Group Variables Template.htm#location_constraints">“location_constraints” on page&#160;1</MadCap:xref>). If this is not done, and a user tries to put an object in one of those buckets (without designating a location at the object level), an error will prevent the PUT object request.</p>
        <p class="Note" MadCap:conditions="PrintGuides.GA7.0">Important: You must have a location called “legacy” that states your pre-existing sproxyd information. If you don't, you will not be able to retrieve your data. For an exaple, see <MadCap:xref href="../2_Deployment_Environment/Modifying the Group Variables Template.htm#location_constraints">“location_constraints” on page&#160;1</MadCap:xref></p>
    </body>
</html>